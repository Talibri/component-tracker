// ==UserScript==
// @name         Talibri - Xmitty's Crafting Tracker
// @namespace    http://talibri.pirion.net/
// @version      0.11b
// @description  Track crafting success.
// @author       Xmitty - Lot of Credit to Kaine "Pirion" Adams for the base work (I learned a lot from his code).
// @match        https://talibri.com/*
// @require      https://gist.github.com/raw/2625891/waitForKeyElements.js
// @require      https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.bundle.js
// @grant        none
// ==/UserScript==

var crafting = {
    data: null,
    is_stopped: false,
    cookiePath: function() {
        return "/";
    },
    cookieName: function() {
        return "/scripts/xmitty/crafting/data";
    },
    cookieExpirationDays: function() {
        return 7;
    },
    initialize: function() {
        //add jquery.cookie.js:
        $('head').append('<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.js"></script>');
        $('head').append('<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>');
        $('head').append('<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" />');
        $('head').append('<script type="text/javascript">var script = script || {};</script>');

        crafting.data = crafting.new();
		crafting.initalizeMenu();
		crafting.initalizeGraph();
        crafting.start();
        window.setInterval(crafting.initalizeMenu, 500);
        window.setInterval(crafting.initalizeGraph, 500);

    },
	initalizeMenu: function() {

        if($(".craftingDropdown").length>0)
        {
            return;
        }

		var menuHtml = '<li class="craftingDropdown">';
		menuHtml += '<a class="craftingDropdown-toggle" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded=false">';
        menuHtml += 'Component Crafting Tracker';
        menuHtml += '<span class="caret"></span>';
        menuHtml += '</a>';
		menuHtml += '<ul class="dropdown-menu">';
		menuHtml += '<li><a>Show Graph: <input type="checkbox" id="componentCheck"> </a></li>';
		menuHtml += '</ul>';
		menuHtml += '</li>';
        $('.navbar-nav').first().append(menuHtml);

        $("#componentCheck").on('click', function () { crafting.showUserInterface(); });
	},
	initalizeGraph: function() {

        if($("#myChart").length>0)
        {
            return;
        }

		var html = "";
			html += '<canvas id="myChart" width="200" height="200"></canvas>';

		$('body').append('<div id="craftingTrackerDialog" title="Crafting Tracking" style="display: none">' + html + '</div>');

		var config = {
			type: 'line',
			data: {
				labels: [0.5,1,1.5,2,2.5,3,3.5,4,4.5,5,5.5,6,6.5,7,7.5,8,8.5,9,9.5,10],
				datasets: [{
					label: "Star Curve",
					data: [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
					backgroundColor: ['rgba(255, 99, 132, 0.5)'],
					borderColor: ['rgba(255,99,132,1)'],
					fill: false,
				}, ]
			},
			options: {
				maintainAspectRatio: false,
				responsive: true,
				layout: {
					padding: {
						top: 10 }
				},
				legend: {
					position: 'bottom',
				},
				tooltips: {
					mode: 'index',
					intersect: false,
				},
				hover: {
					mode: 'nearest',
					intersect: true
				},
				scales: {
					xAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Number Of Stars'
						}
					}],
					yAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Number Of Crafts'
						}
					}]
				}
			}
		};

        var ctx = document.getElementById("myChart").getContext("2d");
        myLine = new Chart(ctx, config);
	},
    showUserInterface: function() {

        var checkBox = document.getElementById('componentCheck');
        if (checkBox.checked === true){
            $("#craftingTrackerDialog").dialog({width: 800, height: 400, title: "My Crafting Tracker"});
        } else {
            $("#craftingTrackerDialog").dialog('close');
        }

    },
    stop: function() {
        crafting.is_stopped = true;
    },
    start: function() {
        //add a listener for any ajax page completed
        $(document).ajaxComplete(crafting.listen);
        crafting.is_stopped = false;
    },
    destory: function() {
        //why would you ever want to do this?
        crafting.stop();
        $.removeCookie(crafting.cookieName(), {path: crafting.cookiePath()});
    },
    reset: function() {
        //overwrite data, and save

        crafting.data = crafting.new();
        crafting.save();
    },
    new: function() {
        //generate an empty json array
        var empty_data = {
            1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0, 8: 0, 9: 0, 10: 0, 11: 0, 12: 0, 13: 0, 14: 0, 15: 0, 16: 0, 17: 0, 18: 0, 19: 0, 20: 0,
            total: 0,
            failure: 0,
        };
        return empty_data;
    },
    load: function() {
        //if cookie exists, load, otherwise get a new array.
        var cookie = $.cookie(crafting.cookieName());
        if(cookie) {
            var loaded_data = $.parseJSON(cookie);
            return crafting.migrate(loaded_data);
        }
        return crafting.new();
    },
    migrate: function(migration_data) {
        return migration_data;
    },
    save: function() {
        //create cookie that expires in 7 days:
        $.cookie(crafting.cookieName(), JSON.stringify(crafting.data), { path: crafting.cookiePath(), expires: crafting.cookieExpirationDays() });
    },
    listen: function(event, xhr, settings) {
        //if we've been asked to exit, let's unbind:
        if(crafting.is_stopped) {
            $(e.currentTarget).unbind('ajaxComplete');
            return;
        }

        //if ajax response comes from expected location:
        if(settings.url.indexOf('component') > -1) {
            if(script && script.reset_component) {
                crafting.reset();
            }
            //if data has not been initialized, initialize it.
            if(!crafting.data) {
                crafting.data = crafting.load();
                //if we can't check the cookie,
                //let's skip this round so we don't overwrite it.
                if(!crafting.data) {
                    return;
                }
            }

            var result = crafting.process(xhr.responseText);

            crafting.logTickResult(result);
        }
    },
    process: function(response) {
        var result = {
            stars: 0,
        };

        var craftingWindow = response.split("\n")[0];
        craftingWindow = craftingWindow.split("html(")[1];
        craftingWindow = craftingWindow.substr(1, craftingWindow.length-4);
        craftingWindow = craftingWindow.replace(/\\n/g,"");
        craftingWindow = craftingWindow.replace(/\\/g,"");
        var craftResult = $(craftingWindow);
        result.stars = ($('.fa-star', craftResult).length * 2 + $('.fa-star-half-o', craftResult).length);

        return result;
    },
    logTickResult: function(result) {
        if(result.stars > 0)
        {
           if(!crafting.data[result.stars]) {
            crafting.data[result.stars] = 1;
            myLine.data.datasets[0].data[result.stars-1] += 1;
           }
           else {
            crafting.data[result.stars] += 1;
            myLine.data.datasets[0].data[result.stars-1] += 1;
           }
           crafting.save();
           myLine.update();
        }
        else if(result.stars == 0)
        {
            crafting.data.failure += 1;

            myLine.update();
        }
        crafting.data.total += 1;
        crafting.data.success = crafting.data.total - crafting.data.failure;
        console.log(crafting.data);
    },
    print: function() {
        console.log(crafting.data);
    }
};

crafting.initialize();